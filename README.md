# pvim
就一个小脚本，将内容放到cfp-vim那里

## 使用
```bash
pvim file type

type: codes,pics

举例:

pvim codes.c codes

pvim 123.png pics
```

脚本依赖一个程序: xclip

感谢提供建议的@farseerfc, @FiveYellowMice, @Haruue Icymoon, @lilydjwg 

## 17.1.7

添加PKGBUILD, 进入AUR

## 17.2.14

添加上传图片功能
